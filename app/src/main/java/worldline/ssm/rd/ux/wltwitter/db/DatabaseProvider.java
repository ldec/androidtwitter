package worldline.ssm.rd.ux.wltwitter.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

/**
 * Created by louis on 16/10/2015.
 */
public class DatabaseProvider extends ContentProvider {

    private  static  final int TWEET_CORRECT_URI_CODE = 42;

    private DataBaseHelper dbHelper;
    private UriMatcher uriMatcher;

    @Override
    public boolean onCreate() {

        dbHelper = new DataBaseHelper(getContext());
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(WLTwitterDatabaseContract.CONTENT_PROVIDER_TWEETS_AUTHORITY, WLTwitterDatabaseContract.TABLE_TWEETS, TWEET_CORRECT_URI_CODE);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {

        Log.i("provider querry", "querry");
        Cursor cursor = dbHelper.getReadableDatabase().query(WLTwitterDatabaseContract.TABLE_TWEETS, strings, s, strings1, s1, null, null);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    public void clearDb(){
        dbHelper = new DataBaseHelper(getContext());
        dbHelper.getReadableDatabase().execSQL("DELETE * FROM tweets");
    }


    @Nullable
    @Override
    public String getType(Uri uri) {

        if(uriMatcher.match(uri)== TWEET_CORRECT_URI_CODE){
            return WLTwitterDatabaseContract.TWEETS_CONTENT_TYPE;
        }
        throw  new IllegalArgumentException("Unknown uri" + uri);
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {

        Log.i("provider insert", "insert");

        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final long rowId = db.insert(WLTwitterDatabaseContract.TABLE_TWEETS, "", contentValues);
        if (rowId > 0) {
            final Uri applicationUri = ContentUris.withAppendedId(WLTwitterDatabaseContract.TWEETS_URI, rowId);
            getContext().getContentResolver().notifyChange(applicationUri, null);
            return applicationUri;
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {

        Log.i("provider delete", "delete");
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int count = db.delete(WLTwitterDatabaseContract.TABLE_TWEETS, s, strings);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {

        Log.i("provider update", "update");
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int count = db.update(WLTwitterDatabaseContract.TABLE_TWEETS, contentValues, s, strings);
        getContext().getContentResolver().notifyChange(uri, null);
        return  0;

    }
}
