package worldline.ssm.rd.ux.wltwitter.remote;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.db.DatabaseProvider;
import worldline.ssm.rd.ux.wltwitter.db.WLTwitterDatabaseContract;
import worldline.ssm.rd.ux.wltwitter.db.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.listeners.TweetListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;
import worldline.ssm.rd.ux.wltwitter.utils.Preferences;

/**
 * Created by louis on 10/23/15.
 */
public class TweetService extends Service implements TweetListener{

    private int nbTweetInserted =0;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //return super.onStartCommand(intent, flags, startId);
        new AsyncTaskGetTwitterFeed(this).execute(Preferences.getLogin());
        Log.d("service", "perdu");
        return Service.START_NOT_STICKY;
    }

    public TweetService() {
        super();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTweetsRetrieved(List<Tweet> tweets) {

        for (Tweet tweet : tweets) {
            final int id = WLTwitterDatabaseManager.insertTweet(tweet);
            if (id > -1) {
                nbTweetInserted++;
            }
        }

        if (nbTweetInserted > 0) {
            final Intent newTweetsIntent = new Intent(Constants.General.ACTION_NEW_TWEETS);
            final Bundle extras = new Bundle();
            extras.putInt(Constants.General.ACTION_NEW_TWEETS_EXTRA, nbTweetInserted);
            newTweetsIntent.putExtras(extras);
            sendBroadcast(newTweetsIntent);
        }

        stopSelf();
    }
}
