package worldline.ssm.rd.ux.wltwitter.listeners;

import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by louis on 14/10/2015.
 */
public interface ActionOnTweetListener {

    void onTweetClick(Tweet tweet);
    void onRt(Tweet tweet);
    void onReply(Tweet tweet);
    void onStar(Tweet tweet);
}
