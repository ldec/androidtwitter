package worldline.ssm.rd.ux.wltwitter.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import worldline.ssm.rd.ux.wltwitter.utils.Constants;
import worldline.ssm.rd.ux.wltwitter.utils.WLTwitterNotification;

/**
 * Created by louis on 28/10/2015.
 */
public class NewTweetsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final int nbNewTweets = intent.getExtras().getInt(Constants.General.ACTION_NEW_TWEETS_EXTRA);
        WLTwitterNotification.displayNewTweetsNotification(nbNewTweets, true, true);
    }
}
