package worldline.ssm.rd.ux.wltwitter.remote;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.listeners.TweetListener;
import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by louis on 02/10/15.
 */
public class AsyncTaskGetTwitterFeed extends AsyncTask<String, Integer, List<Tweet>>{

    TweetListener tweetListener;

    public AsyncTaskGetTwitterFeed( TweetListener tweetListener) {
        super();
        this.tweetListener = tweetListener;
    }

    @Override
    protected List<Tweet> doInBackground(String... strings) {
        String login = strings[0];
        if(!TextUtils.isEmpty(login)){

            Log.d("doInBackground", "Twitter Pull started");
            return TwitterHelper.getTweetsOfUser(login);
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<Tweet> tweets) {
        super.onPostExecute(tweets);

        Log.d("doInBackground", "Twitter Pull finished");
        tweetListener.onTweetsRetrieved(tweets);

    }
}
