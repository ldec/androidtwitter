package worldline.ssm.rd.ux.wltwitter.gui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.List;
import java.util.Objects;

import worldline.ssm.rd.ux.wltwitter.db.WLTwitterDatabaseContract;
import worldline.ssm.rd.ux.wltwitter.db.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.listeners.ActionOnTweetListener;
import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.listeners.TweetListener;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.remote.AsyncTaskGetTwitterFeed;
import worldline.ssm.rd.ux.wltwitter.remote.TweetService;


/**
 * Created by louis on 02/10/15.
 */
public class TweetsFragment extends Fragment implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor>{

    ListView tweetListView;
    ActionOnTweetListener onTweetClickedListener;
    View rootView;
    TweetsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.tweets_fragment_layout, container,false);
        tweetListView = (ListView) rootView.findViewById(R.id.tweetListView);

        adapter = new TweetsAdapter( WLTwitterApplication.getContext(), null, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER,(ActionOnTweetListener) getActivity());
        tweetListView.setAdapter(adapter);

        //WLTwitterDatabaseManager.testContentProvider(tweets);

        tweetListView.setOnItemClickListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        Intent intent = new Intent(WLTwitterApplication.getContext(), TweetService.class);

        WLTwitterApplication.getContext().startService(intent);

        getLoaderManager().initLoader(0, null, this);

        SharedPreferences sharedPreferences = WLTwitterApplication.getContext().getSharedPreferences(getString(R.string.prefFile), Context.MODE_PRIVATE);

        final ProgressBar progressBar = new ProgressBar(getActivity());
        progressBar.setLayoutParams(new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        progressBar.setIndeterminate(true);
        tweetListView.setEmptyView(progressBar);
        ((ViewGroup)rootView).addView(progressBar);

        /*if(!TextUtils.isEmpty(sharedPreferences.getString("login", ""))) {
            new AsyncTaskGetTwitterFeed(this).execute(sharedPreferences.getString("login", ""));
        }*/


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            this.onTweetClickedListener = (ActionOnTweetListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ActionOnTweetListener");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {

        final Cursor cursor =  (Cursor) adapterView.getItemAtPosition(i);
        Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(cursor);
        onTweetClickedListener.onTweetClick(tweet);
    }

    public TweetsFragment() {
        super();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        /*if(cursor != null){
            while(cursor.moveToNext()){
                final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(cursor);
                Log.d("TweetsFragment", tweet.toString());
            }

            if(!cursor.isClosed()){
                cursor.close();
            }
        }*/

        if(adapter != null){
            adapter.changeCursor(cursor);
        }


    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        final CursorLoader cursorLoader = new CursorLoader(WLTwitterApplication.getContext());
        cursorLoader.setUri(WLTwitterDatabaseContract.TWEETS_URI);
        cursorLoader.setProjection(WLTwitterDatabaseContract.PROJECTION_FULL);
        cursorLoader.setSelection(null);
        cursorLoader.setSelectionArgs(null);
        cursorLoader.setSortOrder(null);

        return cursorLoader;
    }
}
