package worldline.ssm.rd.ux.wltwitter;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Calendar;

import worldline.ssm.rd.ux.wltwitter.db.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.listeners.ActionOnTweetListener;
import worldline.ssm.rd.ux.wltwitter.gui.TweetFragment;
import worldline.ssm.rd.ux.wltwitter.gui.TweetsFragment;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.receivers.NewTweetsReceiver;
import worldline.ssm.rd.ux.wltwitter.remote.TweetService;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;


public class WLTwitterActivity extends Activity implements ActionOnTweetListener {

    private PendingIntent pendingIntent;
    private NewTweetsReceiver newTweetsReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String login = getIntent().getExtras().getString("login");
        //String password = getIntent().getExtras().getString("password");

        if(login != null){
            getActionBar().setSubtitle(login);
        }

        // Create fragment

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        TweetsFragment tweetsFragment = new TweetsFragment();
        fragmentTransaction.add(R.id.root, tweetsFragment);
        fragmentTransaction.commit();

    }

    @Override
    protected void onResume() {
        super.onResume();

        final Calendar cal = Calendar.getInstance();
        final Intent serviceIntent = new Intent(this, TweetService.class);
        pendingIntent = PendingIntent.getService(this, 0, serviceIntent, 0);
        final AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        startService(serviceIntent);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), Constants.Twitter.POLL_DELAY, pendingIntent);

        newTweetsReceiver = new NewTweetsReceiver();
        registerReceiver(newTweetsReceiver, new IntentFilter(Constants.General.ACTION_NEW_TWEETS));
    }

    @Override
    protected void onPause() {
        super.onPause();

        final AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        unregisterReceiver(newTweetsReceiver);
        newTweetsReceiver = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actionLogout) {

            SharedPreferences sharedPreferences = WLTwitterApplication.getContext().getSharedPreferences(getString(R.string.prefFile), Context.MODE_PRIVATE);

            sharedPreferences.edit().remove("login");
            sharedPreferences.edit().remove("password");

            sharedPreferences.edit().apply();

            Toast.makeText(getApplicationContext(), "You were logout", Toast.LENGTH_SHORT).show();

            WLTwitterDatabaseManager.dropDatabase();

            this.finish();
        }

        if (id == R.id.actionClearDb) {

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStar(Tweet tweet) {
        Toast.makeText(this, "Star : " + tweet.text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReply(Tweet tweet) {
        Toast.makeText(this, "Reply : " + tweet.text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRt(Tweet tweet) {
        Toast.makeText(this, "RT : " + tweet.text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTweetClick(Tweet tweet) { // User clicked on a tweet, need to display the single tweet fragment

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack("tweet_item_transaction"); // Needed to be able to go back to the tweet list with the back button, if not, app will go back to precedent activity, loginActivity in this case
        fragmentTransaction.replace(R.id.root, TweetFragment.newInstance(tweet));
        fragmentTransaction.commit();

        Log.d("WLTwitterActivity", "Click on a tweet in the list");
    }
}
