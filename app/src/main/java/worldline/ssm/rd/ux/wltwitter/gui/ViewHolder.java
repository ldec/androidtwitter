package worldline.ssm.rd.ux.wltwitter.gui;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import worldline.ssm.rd.ux.wltwitter.R;

/**
 * Created by Adrien on 09/10/2015.
 */
public class ViewHolder {
    public ImageView image;
    public TextView name;
    public TextView alias;
    public TextView text;
    public Button rT;

    public ViewHolder(View view){
        image = (ImageView) view.findViewById(R.id.avatarImageView);
        name = (TextView) view.findViewById(R.id.nameTextView);
        alias = (TextView) view.findViewById(R.id.aliasTextView);
        text = (TextView) view.findViewById(R.id.tweetTextView);
        rT = (Button) view.findViewById(R.id.rTButton);
    }
}
