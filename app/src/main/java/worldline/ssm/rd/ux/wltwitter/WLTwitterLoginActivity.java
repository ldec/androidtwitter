package worldline.ssm.rd.ux.wltwitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * Created by louis on 25/09/15.
 */
public class WLTwitterLoginActivity extends Activity implements OnClickListener {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Context context = WLTwitterApplication.getContext();
        sharedPreferences = context.getSharedPreferences(getString(R.string.prefFile), Context.MODE_PRIVATE);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

        if (!TextUtils.isEmpty(sharedPreferences.getString("login", ""))) {

            Intent loginCheck = new Intent(getApplicationContext(), WLTwitterActivity.class);
            String login = sharedPreferences.getString("login", "");
            String password = sharedPreferences.getString("password", "");

            Bundle extras = new Bundle();
            extras.putString("login", login);
            extras.putString("password", password);
            loginCheck.putExtras(extras);

            startActivity(loginCheck);
        }
    }

    @Override
    public void onClick(View view) {

        EditText loginInput = (EditText) findViewById(R.id.loginInput);
        EditText passwordInput = (EditText) findViewById(R.id.passwordInput);

        if (TextUtils.isEmpty(loginInput.getText())) {
            Toast.makeText(getApplicationContext(), R.string.emptyLogin, Toast.LENGTH_SHORT).show();
        }else {

            if (TextUtils.isEmpty(passwordInput.getText())) {
                Toast.makeText(getApplicationContext(), R.string.emptyPassword, Toast.LENGTH_SHORT).show();

            } else {

                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString("login", loginInput.getText().toString());
                editor.putString("password", passwordInput.getText().toString());
                editor.apply();

                Intent loginCheck = new Intent(getApplicationContext(), WLTwitterActivity.class);

                Bundle extras = new Bundle();
                extras.putString("login", loginInput.getText().toString());
                extras.putString("password", passwordInput.getText().toString());
                loginCheck.putExtras(extras);

                loginInput.setText("");
                loginInput.requestFocus();
                passwordInput.setText("");

                startActivity(loginCheck);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actionLogout) {

            SharedPreferences sharedPreferences = WLTwitterApplication.getContext().getSharedPreferences(getString(R.string.prefFile), Context.MODE_PRIVATE);

            sharedPreferences.edit().remove("login");
            sharedPreferences.edit().remove("password");

            sharedPreferences.edit().apply();

            Toast.makeText(getApplicationContext(), R.string.logout, Toast.LENGTH_SHORT).show();

            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}