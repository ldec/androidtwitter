package worldline.ssm.rd.ux.wltwitter.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.pojo.TwitterUser;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class WLTwitterDatabaseManager {

	public static Tweet tweetFromCursor(Cursor c){
		if (null != c){
			final Tweet tweet = new Tweet();
			tweet.user = new TwitterUser();

			// Retrieve the date created
			if (c.getColumnIndex(WLTwitterDatabaseContract.DATE_CREATED) >= 0){
				tweet.dateCreated = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.DATE_CREATED));
			}

			// Retrieve the user name
			if (c.getColumnIndex(WLTwitterDatabaseContract.USER_NAME) >= 0){
				tweet.user.name = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.USER_NAME));
			}

			// Retrieve the user alias
			if (c.getColumnIndex(WLTwitterDatabaseContract.USER_ALIAS) >= 0){
				tweet.user.screenName = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.USER_ALIAS));
			}

			// Retrieve the user image url
			if (c.getColumnIndex(WLTwitterDatabaseContract.USER_IMAGE_URL) >= 0){
				tweet.user.profileImageUrl = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.USER_IMAGE_URL));
			}

			// Retrieve the text of the tweet
			if (c.getColumnIndex(WLTwitterDatabaseContract.TEXT) >= 0){
				tweet.text = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.TEXT));
			}

			return tweet;
		}
		return null;
	}

	public static ContentValues tweetToContentValues(Tweet tweet){
		final ContentValues values = new ContentValues();

		// Set the date created
		if (!TextUtils.isEmpty(tweet.dateCreated)){
			values.put(WLTwitterDatabaseContract.DATE_CREATED, tweet.dateCreated);
		}

		// Set the date created as timestamp
		values.put(WLTwitterDatabaseContract.DATE_CREATED_TIMESTAMP, tweet.getDateCreatedTimestamp());
		
		// Set the user name
		if (!TextUtils.isEmpty(tweet.user.name)){
			values.put(WLTwitterDatabaseContract.USER_NAME, tweet.user.name);
		}

		// Set the user alias
		if (!TextUtils.isEmpty(tweet.user.screenName)){
			values.put(WLTwitterDatabaseContract.USER_ALIAS, tweet.user.screenName);
		}

		// Set the user image url
		if (!TextUtils.isEmpty(tweet.user.profileImageUrl)){
			values.put(WLTwitterDatabaseContract.USER_IMAGE_URL, tweet.user.profileImageUrl);
		}

		// Set the text of the tweet
		if (!TextUtils.isEmpty(tweet.text)){
			values.put(WLTwitterDatabaseContract.TEXT, tweet.text);
		}

		return values;
	}

	public static synchronized int insertTweet(Tweet tweet){
		if (null != tweet){
			if (!doesContainTweet(tweet)){
				final Uri uri = WLTwitterApplication.getContext().getContentResolver().insert(
						WLTwitterDatabaseContract.TWEETS_URI, tweetToContentValues(tweet));
				if (null != uri){
					return Integer.parseInt(uri.getLastPathSegment());
				} else {
					return -1;
				}
			}
		}
		return -1;
	}

	public static synchronized List<Tweet> getStoredTweets(){
		final List<Tweet> tweets = new ArrayList<Tweet>();
		final Cursor cursor = WLTwitterApplication.getContext().getContentResolver().query(
				WLTwitterDatabaseContract.TWEETS_URI, WLTwitterDatabaseContract.PROJECTION_FULL, null, null, null);
		if (null != cursor){
			while (cursor.moveToNext()){
				tweets.add(tweetFromCursor(cursor));
			}
		}
		if ((null != cursor) && (!cursor.isClosed())) {
			cursor.close();
		}
		return tweets;
	}

	public static synchronized void dropDatabase(){
		WLTwitterApplication.getContext().getContentResolver().delete(
				WLTwitterDatabaseContract.TWEETS_URI, null, null);
	}

	private static synchronized boolean doesContainTweet(Tweet tweet){
		boolean result = false;
		if ((null != tweet) && (!TextUtils.isEmpty(tweet.dateCreated))){
			final Cursor cursor = WLTwitterApplication.getContext().getContentResolver().query(
					WLTwitterDatabaseContract.TWEETS_URI, WLTwitterDatabaseContract.PROJECTION_FULL,
					WLTwitterDatabaseContract.SELECTION_BY_CREATION_DATE, new String[]{tweet.dateCreated}, null);
			if ((null != cursor) && (cursor.moveToFirst())) {
				result = true;
			}
			if ((null != cursor) && (!cursor.isClosed())) {
				cursor.close();
			}
		}
		return result;
	}

	public static void testDatabase(List<Tweet> tweets){

		/*final SQLiteOpenHelper sqLiteOpenHelper = new DataBaseHelper(WLTwitterApplication.getContext());
		final SQLiteDatabase tweetsDb = sqLiteOpenHelper.getWritableDatabase();

		for(Tweet tweet : tweets){
			final ContentValues contentValues = new ContentValues();
			contentValues.put(WLTwitterDatabaseContract.USER_NAME, tweet.user.name);
			contentValues.put(WLTwitterDatabaseContract.USER_ALIAS, tweet.user.screenName);
			contentValues.put(WLTwitterDatabaseContract.USER_IMAGE_URL, tweet.user.profileImageUrl);
			contentValues.put(WLTwitterDatabaseContract.TEXT, tweet.text);
			contentValues.put(WLTwitterDatabaseContract.DATE_CREATED, tweet.dateCreated);

			tweetsDb.insert(WLTwitterDatabaseContract.TABLE_TWEETS, "", contentValues);

		}

		final Cursor cursor = tweetsDb.query(WLTwitterDatabaseContract.TABLE_TWEETS, WLTwitterDatabaseContract.PROJECTION_FULL, null,null,null,null,null);

		while (cursor.moveToNext()){
			final String tweetUserName = cursor.getString(cursor.getColumnIndex(WLTwitterDatabaseContract.USER_NAME));
			final String tweetText = cursor.getString(cursor.getColumnIndex(WLTwitterDatabaseContract.TEXT));
		}

		if(!cursor.isClosed()){
			cursor.close();
		}
		*/
	}

	public static void testContentProvider(List<Tweet> tweets){

		for (Tweet tweet : tweets) {
			WLTwitterApplication.getContext().getContentResolver().insert(WLTwitterDatabaseContract.TWEETS_URI, tweetToContentValues(tweet));
		}

		Tweet testTweet = createFakeTweet();
        Tweet testTweet2 = createFakeTweet();

		WLTwitterApplication.getContext().getContentResolver().insert(WLTwitterDatabaseContract.TWEETS_URI, tweetToContentValues(testTweet));
		WLTwitterApplication.getContext().getContentResolver().insert(WLTwitterDatabaseContract.TWEETS_URI, tweetToContentValues(testTweet2));


		ContentValues contentValues = new ContentValues();
		contentValues.put(WLTwitterDatabaseContract.TEXT, "Test Tweet text 123 456 (modified)");

		WLTwitterApplication.getContext().getContentResolver().update(WLTwitterDatabaseContract.TWEETS_URI, contentValues, WLTwitterDatabaseContract.SELECTION_BY_USER_NAME, new String[]{testTweet.user.name});

        WLTwitterApplication.getContext().getContentResolver().delete(WLTwitterDatabaseContract.TWEETS_URI, WLTwitterDatabaseContract.SELECTION_BY_CREATION_DATE, new String[]{testTweet2.dateCreated});

    }

    public static Tweet createFakeTweet(){
		Tweet tweet = new Tweet();

		tweet.user = new TwitterUser();
		tweet.user.name = "TestTweetUserName";
		tweet.user.screenName = "TestTweetScreenName";
		tweet.user.profileImageUrl = "http://vignette2.wikia.nocookie.net/fallout/images/7/73/Trollface.png/revision/latest?cb=20120710173828";

		tweet.dateCreated = new Date().toString();

		tweet.text = "Test Tweet text 123";

		return tweet;
	}

}
