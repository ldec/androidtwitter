package worldline.ssm.rd.ux.wltwitter.utils;

import android.content.Context;
import android.content.SharedPreferences;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;

/**
 * Created by louis on 10/23/15.
 */
public class Preferences {

    public static String getLogin() {

        SharedPreferences sharedPreferences = WLTwitterApplication.getContext().getSharedPreferences(WLTwitterApplication.getContext().getString(R.string.prefFile), Context.MODE_PRIVATE);

        return sharedPreferences.getString("login", "");
    }
}
