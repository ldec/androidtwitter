package worldline.ssm.rd.ux.wltwitter.gui;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;

import com.squareup.picasso.Picasso;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.db.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.listeners.ActionOnTweetListener;
import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by louis on 09/10/15.
 */
public class TweetsAdapter extends CursorAdapter implements View.OnClickListener{

    Cursor cursor;
    LayoutInflater inflater;
    ActionOnTweetListener actionOnTweetListener;

    public TweetsAdapter (Context context, Cursor cursor, int flags, ActionOnTweetListener actionOnTweetListener){
        super(context,cursor,flags);
        /*this.inflater = LayoutInflater.from(WLTwitterApplication.getContext());*/
        this.actionOnTweetListener = actionOnTweetListener;
    }

    /*@Override
    public Object getItem(int i) {
        return this.tweetList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View tweetView, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (null == tweetView){

            tweetView = inflater.inflate(R.layout.tweet_list_item_layout,null);

            viewHolder = new ViewHolder(tweetView);

            tweetView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) tweetView.getTag();
        }
        final Tweet tweet = (Tweet) getItem(i);

        viewHolder.name.setText(tweet.user.name);
        viewHolder.alias.setText(tweet.user.screenName);
        viewHolder.text.setText(tweet.text);

        Picasso.with(WLTwitterApplication.getContext()).load(tweet.user.profileImageUrl).into(viewHolder.image);

        viewHolder.rT.setTag(tweet);
        viewHolder.rT.setOnClickListener(this);

        return tweetView;
    }*/

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        final View view = LayoutInflater.from(context).inflate(R.layout.tweet_list_item_layout, null);
        final ViewHolder holder = new ViewHolder(view);

        final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(cursor);
        holder.name.setText(tweet.user.name);
        holder.alias.setText(tweet.user.screenName);
        holder.text.setText(tweet.text);

        Picasso.with(WLTwitterApplication.getContext()).load(tweet.user.profileImageUrl).into(holder.image);

        holder.rT.setTag(tweet);
        holder.rT.setOnClickListener(this);

        view.setTag(holder);


        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final  ViewHolder holder = (ViewHolder) view.getTag();

        final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(cursor);

        holder.name.setText(tweet.user.name);
        holder.alias.setText(tweet.user.screenName);
        holder.text.setText(tweet.text);

        Picasso.with(WLTwitterApplication.getContext()).load(tweet.user.profileImageUrl).into(holder.image);

        holder.rT.setTag(tweet);
        holder.rT.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Tweet tag = (Tweet) v.getTag();
        actionOnTweetListener.onRt(tag);
    }
}
