package worldline.ssm.rd.ux.wltwitter.gui;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import worldline.ssm.rd.ux.wltwitter.listeners.ActionOnTweetListener;
import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by louis on 14/10/2015.
 */
public class TweetFragment extends Fragment implements View.OnClickListener {

    private Tweet tweet;
    private ActionOnTweetListener actionOnTweetListener;

    public static TweetFragment newInstance(Tweet tweet) {
        final TweetFragment tweetFragment = new TweetFragment();
        final Bundle bundle = new Bundle();

        bundle.putParcelable("tweet", tweet);
        tweetFragment.setArguments(bundle);
        return tweetFragment;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof ActionOnTweetListener){
            actionOnTweetListener = (ActionOnTweetListener) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement ActionOnTweetListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tweet, container, false);
        tweet = getArguments().getParcelable("tweet");

        final TextView fullPseudo = (TextView) rootView.findViewById(R.id.tweetUserName);
        fullPseudo.setText(tweet.user.name);

        final TextView pseudo = (TextView) rootView.findViewById(R.id.tweetUserAlias);
        pseudo.setText("@" + tweet.user.screenName);

        final TextView text = (TextView) rootView.findViewById(R.id.tweetText);
        text.setText(tweet.text);

        final ImageView profilePic = (ImageView) rootView.findViewById(R.id.tweetProfilPic);

        Picasso.with(getActivity().getApplicationContext()).load(tweet.user.profileImageUrl).into(profilePic);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        View rootView = getView();

        if (rootView != null) {
            Button replyButton = (Button) rootView.findViewById(R.id.tweetReplyButton);
            replyButton.setOnClickListener(this);

            Button rtButton = (Button) rootView.findViewById(R.id.tweetRtButton);
            rtButton.setOnClickListener(this);

            Button starButton = (Button) rootView.findViewById(R.id.tweetStarButton);
            starButton.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tweetStarButton:
                actionOnTweetListener.onStar(tweet);
                break;
            case R.id.tweetReplyButton:
                actionOnTweetListener.onReply(tweet);
                break;
            case R.id.tweetRtButton:
                actionOnTweetListener.onRt(tweet);
                break;
        }
    }
}
